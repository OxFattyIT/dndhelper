
using Microsoft.EntityFrameworkCore;
using DnDPlayerReference.Models;

namespace DnDPlayerReference.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Player> Players { get; set; }
    }
}